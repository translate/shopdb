package main

import (
	"flag"
	"fmt"
	"io"
	"os"
	"runtime"
	"runtime/debug"

	"mellium.im/cli"
)

func aboutCmd(w io.Writer, version string) *cli.Command {
	if w == nil {
		panic("nil writer in about command")
	}

	var (
		verbose bool
	)
	flags := flag.NewFlagSet("migrate", flag.ContinueOnError)
	flags.BoolVar(&verbose, "v", verbose, "Dump verbose build information.")

	info, infoOK := debug.ReadBuildInfo()

	return &cli.Command{
		Usage:       "about",
		Description: "Show information about this binary.",
		Flags:       flags,
		Run: func(c *cli.Command, _ ...string) error {
			fmt.Fprintf(w, `shopdb (%s)

version:     %s
go version:  %s
go compiler: %s
platform:    %s/%s
btf mode:    %t
dev mode:    %t
`,
				os.Args[0],
				version,
				info.GoVersion, runtime.Compiler, runtime.GOOS, runtime.GOARCH,
				BTF, Dev)
			if verbose && infoOK {
				fmt.Fprintf(w, "\nBuild info:\n%s", info.String())
			}
			return nil
		},
	}
}
