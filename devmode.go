//go:build !dev
// +build !dev

package main

import (
	"embed"
	"io/fs"
)

// Dev is set at compile time by the "dev" build tag and indicates whether the
// binary was built in dev mode or not.
const Dev = false

//go:embed migrations
var migrations embed.FS

// In production mode, embed the migrations on build.
func getMigrationsFS() fs.FS {
	vfs, err := fs.Sub(migrations, "migrations")
	if err != nil {
		panic(err)
	}
	return vfs
}
