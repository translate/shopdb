// Package store contains types for dealing with the database.
package store

import (
	"context"
	"database/sql"
	"fmt"
)

// DB is a collection of all the various data stores.
type DB struct {
	Bikes
	Customers
	Inventory
	WorkOrder
	Login
	User
}

func New(ctx context.Context, db *sql.DB, sessionKey string) (*DB, error) {
	bikes, err := NewBikes(ctx, db)
	if err != nil {
		return nil, fmt.Errorf("failed to prepare bike statements: %v", err)
	}
	customers, err := NewCustomers(ctx, db)
	if err != nil {
		return nil, fmt.Errorf("failed to prepare customer statements: %v", err)
	}
	workorders, err := NewWorkOrders(ctx, db)
	if err != nil {
		return nil, fmt.Errorf("failed to prepare WO statements: %v", err)
	}
	inventory, err := NewInventory(ctx, db)
	if err != nil {
		return nil, fmt.Errorf("failed to prepare inventory statements: %v", err)
	}
	login, err := NewLogin(ctx, db, sessionKey)
	if err != nil {
		return nil, fmt.Errorf("failed to prepare login statements: %v", err)
	}
	user, err := NewUser(ctx, db)
	if err != nil {
		return nil, fmt.Errorf("failed to prepare user statements: %v", err)
	}

	return &DB{
		Bikes:     bikes,
		Customers: customers,
		Inventory: inventory,
		WorkOrder: workorders,
		Login:     login,
		User:      user,
	}, nil
}
