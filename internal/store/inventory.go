package store

import (
	"context"
	"database/sql"
	"errors"
	"fmt"

	"code.soquee.net/shopdb/internal/graph/model"
)

// Inventory performs operations on the inventory table.
type Inventory struct {
	db      *sql.DB
	getItem *sql.Stmt
	newItem *sql.Stmt
}

// NewInventory creates a new Inventory that performs its operations on the
// given database.
func NewInventory(ctx context.Context, db *sql.DB) (Inventory, error) {
	var err error
	store := Inventory{
		db: db,
	}

	store.getItem, err = db.PrepareContext(ctx, `
SELECT name,barcode,cost,variable,archived,created_at,updated_at
	FROM inventory
	WHERE id=$1`)
	if err != nil {
		return store, fmt.Errorf("error preparing getItem: %q", err)
	}
	store.newItem, err = db.PrepareContext(ctx, `
INSERT INTO inventory (name,barcode,cost,variable,archived)
	VALUES ($1, $2, $3, $4, $5)
	RETURNING id, created_at, updated_at`)
	if err != nil {
		return store, fmt.Errorf("error preparing newItem: %q", err)
	}

	return store, nil
}

// DB returns the database that the store was created with.
func (s Inventory) DB() *sql.DB {
	return s.db
}

// GetItem returns the given item from the inventory.
func (s Inventory) GetItem(ctx context.Context, id int) (*model.Item, error) {
	item := &model.Item{
		ID: id,
	}
	err := s.getItem.QueryRowContext(ctx, id).Scan(
		&item.Name, &item.Barcode, &item.Cost, &item.Variable, &item.Archived,
		&item.Created, &item.Updated)
	if err != nil && errors.Is(err, sql.ErrNoRows) {
		return nil, fmt.Errorf("no item with ID %d", id)
	}
	return item, err
}

// NewItem creates an item and returns the created item.
func (s Inventory) NewItem(ctx context.Context, newItem model.NewItem) (*model.Item, error) {
	item := &model.Item{
		Name:    newItem.Name,
		Barcode: newItem.Barcode,
		Cost:    newItem.Cost,
	}
	if newItem.Variable != nil {
		item.Variable = *newItem.Variable
	}
	if newItem.Archived != nil {
		item.Archived = *newItem.Archived
	}
	err := s.newItem.QueryRowContext(ctx, newItem.Name, newItem.Barcode, newItem.Cost, newItem.Variable, newItem.Archived).
		Scan(&item.ID, &item.Created, &item.Updated)
	return item, err
}
