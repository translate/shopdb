package store

import (
	"context"
	"database/sql"
	"fmt"

	"code.soquee.net/login"
)

// User performs operations on the users table.
type User struct {
	db                  *sql.DB
	fetchPassHash       *sql.Stmt
	fetchPassHashByUser *sql.Stmt
	newUser             *sql.Stmt
	upgradeScheme       *sql.Stmt
	lockAccount         *sql.Stmt
	emailInfo           *sql.Stmt
	checkEmail          *sql.Stmt
	getUID              *sql.Stmt
	updateEmail         *sql.Stmt
	setTOTP             *sql.Stmt
	getTOTP             *sql.Stmt
}

// NewUser creates a new User store that performs its operations on the given
// database.
func NewUser(ctx context.Context, db *sql.DB) (User, error) {
	var err error
	store := User{
		db: db,
	}

	store.newUser, err = db.PrepareContext(ctx, `
	INSERT INTO users AS u (username, displayname, email, email_secret, salt, passHash, hashscheme, totp_secret)
		VALUES ($1, $2, $3, $4, $5, $6, $7, $8)
		RETURNING u.id AS id`)
	if err != nil {
		return store, err
	}

	store.fetchPassHash, err = db.PrepareContext(ctx, `
	SELECT id, salt, passHash, hashscheme, totp_enabled
		FROM users
		WHERE email=$1;`)
	if err != nil {
		return store, err
	}

	store.emailInfo, err = db.PrepareContext(ctx, `
	SELECT username, email, email_secret
		FROM users
		WHERE id=$1`)
	if err != nil {
		return store, err
	}

	store.fetchPassHashByUser, err = db.PrepareContext(ctx, `
	SELECT u.username, u.email, u.salt, u.passHash, u.hashscheme
		FROM users AS u
		WHERE u.id=$1;`)
	if err != nil {
		return store, err
	}

	store.checkEmail, err = db.PrepareContext(ctx, `
	SELECT EXISTS (SELECT 1 FROM users WHERE email=$1)`)
	if err != nil {
		return store, err
	}

	store.getUID, err = db.PrepareContext(ctx, `
	SELECT id FROM users WHERE email=$1`)
	if err != nil {
		return store, err
	}

	store.updateEmail, err = db.PrepareContext(ctx, `
	UPDATE ONLY users
		SET email=$2, verified=FALSE
		WHERE email=$1`)
	if err != nil {
		return store, err
	}

	store.lockAccount, err = db.PrepareContext(ctx, `
	UPDATE ONLY users
		SET passHash='', hashscheme=0
		WHERE id=$1;`)
	if err != nil {
		return store, err
	}

	store.setTOTP, err = db.PrepareContext(ctx, `
	UPDATE users SET totp_enabled=$2 WHERE id=$1`)
	if err != nil {
		return store, err
	}

	store.getTOTP, err = db.PrepareContext(ctx, `
	SELECT email, verified, totp_enabled, totp_secret FROM users WHERE id=$1`)
	if err != nil {
		return store, err
	}

	store.upgradeScheme, err = db.PrepareContext(ctx, `
	UPDATE ONLY users
		SET salt=$1, passHash=$2, hashscheme=$3
		WHERE email=$4;`)
	return store, err
}

// DB returns the database that the store was created with.
func (s User) DB() *sql.DB {
	return s.db
}

// NewUser inserts a user into the database and returns the user's ID.
func (s User) NewUser(ctx context.Context, displayname, username, email string, emailSecret, totpSecret, salt, passHash []byte, scheme int) (uid int, err error) {
	err = s.newUser.QueryRowContext(
		ctx, username, displayname, email, emailSecret, salt, passHash, scheme, totpSecret,
	).Scan(&uid)
	if err != nil {
		return 0, err
	}

	return uid, nil
}

// GetEmailVerifyInfo returns the username and email secret of a user with the given ID.
func (s User) GetEmailVerifyInfo(ctx context.Context, tx *sql.Tx, uid int) (username, email string, emailSecret []byte, err error) {
	var row *sql.Row
	if tx == nil {
		row = s.emailInfo.QueryRowContext(ctx, uid)
	} else {
		row = tx.Stmt(s.emailInfo).QueryRowContext(ctx, uid)
	}
	err = row.Scan(&username, &email, &emailSecret)
	return username, email, emailSecret, err
}

// CheckDuplicateEmail looks to see if the email is already associated with a
// user account.
//
// Any function like this is subject to race conditions, but it's likely okay to
// use it to try and make the users experience nicer (and if there is a race, it
// will hard fail on the unique constraint).
func (s User) CheckDuplicateEmail(ctx context.Context, email string) (exists bool, err error) {
	err = s.checkEmail.QueryRowContext(ctx, email).Scan(&exists)
	return exists, err
}

// FetchPassHash is a fetches a password hash, username and salt given an email.
// If no such email exists, db.ErrNoRows is returned.
func (s User) FetchPassHash(ctx context.Context, email string) (uid int, salt, passHash []byte, scheme int, totp bool, err error) {
	err = s.fetchPassHash.QueryRowContext(ctx, email).Scan(&uid, &salt, &passHash, &scheme, &totp)
	return uid, salt, passHash, scheme, totp, err
}

// FetchPassHashByUser is a fetches a password hash, username and salt given a
// uid.
// If no such uid exists, db.ErrNoRows is returned.
func (s User) FetchPassHashByUser(ctx context.Context, uid int) (username, email string, salt, passHash []byte, scheme int, err error) {
	err = s.fetchPassHashByUser.QueryRowContext(ctx, uid).Scan(&username, &email, &salt, &passHash, &scheme)
	return username, email, salt, passHash, scheme, err
}

// UpgradeScheme updates the password storage scheme when provided with the
// password itself (not the hash) and the current scheme.
//
// If the current scheme is the latest one, no database query is issued.
func (s User) UpgradeScheme(ctx context.Context, salt, password []byte, scheme int, email string) error {
	password, scheme = login.Update(scheme, password, salt)
	if len(password) > 0 {
		_, err := s.upgradeScheme.ExecContext(ctx, salt, password, scheme, email)
		return err
	}
	return nil
}

// LockAccount locks the account so that it cannot be used for login by removing
// the password entirely.
func (s User) LockAccount(ctx context.Context, tx *sql.Tx, uid int) error {
	if tx == nil {
		_, err := s.lockAccount.ExecContext(ctx, uid)
		return err
	}
	_, err := tx.Stmt(s.lockAccount).ExecContext(ctx, uid)
	return err
}

// HashEmail replaces the email with a hashed version of itself.
// This is so that we don't retain personally identifiable information when the
// user deletes their account, but if we ever want to re-associate new accounts
// with old posts made in the past we can compare the hashes.
func (s User) HashEmail(ctx context.Context, tx *sql.Tx, salt []byte, email string) error {
	hash, scheme := login.Update(-1, []byte(email), salt)
	final := fmt.Sprintf("%d:%x", scheme, hash)
	if tx == nil {
		_, err := s.updateEmail.ExecContext(ctx, email, final)
		return err
	}
	_, err := tx.Stmt(s.updateEmail).ExecContext(ctx, email, final)
	return err
}

// GetUID gets the users ID from their email.
func (s User) GetUID(ctx context.Context, email string) (uid int, err error) {
	err = s.getUID.QueryRowContext(ctx, email).Scan(&uid)
	return uid, err
}

// EnableTOTP marks two factor auth as enabled in the database.
func (s User) EnableTOTP(ctx context.Context, uid int) error {
	_, err := s.setTOTP.ExecContext(ctx, uid, true)
	return err
}

// DisableTOTP marks two factor auth as disabled in the database.
func (s User) DisableTOTP(ctx context.Context, uid int) error {
	_, err := s.setTOTP.ExecContext(ctx, uid, false)
	return err
}

// GetTOTP returns information about TOTP based multi-factor authentication.
func (s User) GetTOTP(ctx context.Context, uid int) (enabled bool, email string, verified bool, secret []byte, err error) {
	err = s.getTOTP.QueryRowContext(ctx, uid).Scan(&email, &verified, &enabled, &secret)
	return enabled, email, verified, secret, err
}
