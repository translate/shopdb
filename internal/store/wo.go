package store

import (
	"context"
	"database/sql"
	"errors"
	"fmt"

	"code.soquee.net/shopdb/internal/graph/model"
)

// WorkOrder performs operations on the workorders and lineitems tables.
type WorkOrder struct {
	db                  *sql.DB
	getService          *sql.Stmt
	getServiceLineItems *sql.Stmt
	getPartLineItems    *sql.Stmt
	getNotes            *sql.Stmt
	getWO               *sql.Stmt

	createService         *sql.Stmt
	createNote            *sql.Stmt
	createServiceLineItem *sql.Stmt
	createPartLineItem    *sql.Stmt
	createWONoBike        *sql.Stmt
	createWOBike          *sql.Stmt
}

// NewWorkOrders creates a new WorkOrder that performs its operations on the
// given database.
func NewWorkOrders(ctx context.Context, db *sql.DB) (WorkOrder, error) {
	var err error
	store := WorkOrder{
		db: db,
	}

	store.getService, err = db.PrepareContext(ctx, `
SELECT id,name,cost,dur,hourly,expl,archived,created_at,updated_at FROM services WHERE id=$1`)
	if err != nil {
		return store, fmt.Errorf("error preparing getService: %q", err)
	}
	store.getServiceLineItems, err = db.PrepareContext(ctx, `
SELECT li.id,li.service,li.dur,li.discount,li.final_cost,li.created_at,li.updated_at FROM servicelineitems li
	LEFT JOIN workorders AS wo ON li.wo=wo.id
	WHERE wo.num=$1`)
	if err != nil {
		return store, fmt.Errorf("error preparing getServiceLineItems: %q", err)
	}
	store.getPartLineItems, err = db.PrepareContext(ctx, `
SELECT li.id,li.item,li.cost,li.discount,li.final_cost,li.created_at,li.updated_at FROM partlineitems li
	LEFT JOIN workorders AS wo ON li.wo=wo.id
	WHERE wo.num=$1`)
	if err != nil {
		return store, fmt.Errorf("error preparing getPartLineItems: %q", err)
	}
	store.getNotes, err = db.PrepareContext(ctx, `
SELECT n.note,n.created_at
	FROM workordernotes n
	LEFT JOIN workorders AS wo ON (n.wo=wo.id)
	WHERE wo.num=$1`)
	if err != nil {
		return store, fmt.Errorf("error preparing getNotes: %q", err)
	}
	store.getWO, err = db.PrepareContext(ctx, `
SELECT num,status,req,note,created_at,updated_at
	FROM workorders WHERE num=$1`)
	if err != nil {
		return store, fmt.Errorf("error preparing getWO: %q", err)
	}

	store.createService, err = db.PrepareContext(ctx, `
INSERT INTO services (name, cost, dur, hourly, expl, archived)
	VALUES ($1, $2, $3, $4, $5, $6)
	RETURNING id,created_at,updated_at`)
	if err != nil {
		return store, fmt.Errorf("error preparing createService: %q", err)
	}
	store.createNote, err = db.PrepareContext(ctx, `
INSERT INTO workordernotes (wo, note)
	SELECT id, $2 FROM workorders WHERE num=$1
	RETURNING id,created_at`)
	if err != nil {
		return store, fmt.Errorf("error preparing createNote: %q", err)
	}
	store.createServiceLineItem, err = db.PrepareContext(ctx, `
INSERT INTO servicelineitems (wo, service, dur, discount, final_cost)
	SELECT id, $2, $3, $4, $5 FROM workorders WHERE num=$1
	RETURNING id, created_at, updated_at
`)
	if err != nil {
		return store, fmt.Errorf("error preparing createServiceLineItem: %q", err)
	}
	store.createPartLineItem, err = db.PrepareContext(ctx, `
INSERT INTO partlineitems (wo, item, cost, discount, final_cost)
	SELECT id, $2, $3, $4, $5 FROM workorders WHERE num=$1
	RETURNING id, created_at, updated_at
`)
	if err != nil {
		return store, fmt.Errorf("error preparing createPartLineItem: %q", err)
	}
	store.createWOBike, err = db.PrepareContext(ctx, `
INSERT INTO workorders (status, bike, customer, req, note)
	SELECT $1, b.id, c.id, $4, COALESCE($5, '') FROM customers c, bikes b WHERE c.num=$3 AND b.num=$2
	RETURNING num, created_at, updated_at`)
	if err != nil {
		return store, fmt.Errorf("error preparing createWO: %q", err)
	}
	store.createWONoBike, err = db.PrepareContext(ctx, `
INSERT INTO workorders (status, customer, req, note)
	SELECT $1, c.id, $3, COALESCE($4, '') FROM customers c WHERE c.num=$2
	RETURNING num, created_at, updated_at`)
	if err != nil {
		return store, fmt.Errorf("error preparing createWO: %q", err)
	}

	return store, nil
}

// DB returns the database that the store was created with.
func (s WorkOrder) DB() *sql.DB {
	return s.db
}

// GetWO returns the given work order
func (s WorkOrder) GetWO(ctx context.Context, id int) (*model.WorkOrder, error) {
	wo := &model.WorkOrder{}
	err := s.getWO.QueryRowContext(ctx, id).Scan(
		&wo.ID, &wo.Status, &wo.CustReq, &wo.NoteToCust, &wo.Created, &wo.Updated)
	if err != nil && errors.Is(err, sql.ErrNoRows) {
		return nil, nil
	}
	return wo, err
}

// GetService returns the given service.
func (s WorkOrder) GetService(ctx context.Context, id int) (*model.Service, error) {
	svc := &model.Service{}
	err := s.getService.QueryRowContext(ctx, id).Scan(&svc.ID, &svc.Name, &svc.Cost, &svc.Dur, &svc.Hourly, &svc.Expl, &svc.Archived, &svc.Created, &svc.Updated)
	if err != nil && errors.Is(err, sql.ErrNoRows) {
		return nil, nil
	}
	return svc, err
}

// GetServiceLineItems returns all of the line items on the given work order
// that represent services to be performed.
func (s WorkOrder) GetServiceLineItems(ctx context.Context, id int) ([]*model.ServiceLineItem, error) {
	lis := []*model.ServiceLineItem{}
	rows, err := s.getServiceLineItems.QueryContext(ctx, id)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, fmt.Errorf("work order %d does not exist or an error occured", id)
		}
		return nil, err
	}
	defer rows.Close()
	for rows.Next() {
		li := &model.ServiceLineItem{
			Service: &model.Service{},
		}
		err = rows.Scan(
			&li.ID, &li.Service.ID, &li.Dur, &li.Discount, &li.FinalCost, &li.Created, &li.Updated)
		if err != nil {
			return nil, err
		}
		lis = append(lis, li)
	}
	return lis, nil
}

// GetPartLineItems returns all of the line items on the given work order
// that represent services to be performed.
func (s WorkOrder) GetPartLineItems(ctx context.Context, id int) ([]*model.PartLineItem, error) {
	lis := []*model.PartLineItem{}
	rows, err := s.getPartLineItems.QueryContext(ctx, id)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, fmt.Errorf("work order %d does not exist or an error occured", id)
		}
		return nil, err
	}
	defer rows.Close()
	for rows.Next() {
		li := &model.PartLineItem{
			Item: &model.Item{},
		}
		err = rows.Scan(
			&li.ID, &li.Item.ID, &li.Cost, &li.Discount, &li.FinalCost, &li.Created, &li.Updated)
		if err != nil {
			return nil, err
		}
		lis = append(lis, li)
	}
	return lis, nil
}

// GetNotes returns all of the internal notes for the given work order.
func (s WorkOrder) GetNotes(ctx context.Context, id int) ([]*model.Note, error) {
	notes := []*model.Note{}
	rows, err := s.getNotes.QueryContext(ctx, id)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, nil
		}
		return nil, err
	}
	defer rows.Close()
	for rows.Next() {
		note := &model.Note{}
		err = rows.Scan(&note.Note, &note.Created)
		if err != nil {
			return nil, err
		}
		notes = append(notes, note)
	}
	return notes, nil
}

// CreateService adds a new service to the menu.
func (s WorkOrder) CreateService(ctx context.Context, newSvc model.NewService) (*model.Service, error) {
	svc := &model.Service{
		Name:     newSvc.Name,
		Cost:     newSvc.Cost,
		Dur:      newSvc.Dur,
		Expl:     newSvc.Expl,
		Hourly:   *newSvc.Hourly,
		Archived: *newSvc.Archived,
	}
	err := s.createService.
		QueryRowContext(ctx,
			newSvc.Name, newSvc.Cost, newSvc.Dur, newSvc.Hourly, newSvc.Expl,
			newSvc.Archived).
		Scan(
			&svc.ID, &svc.Created, &svc.Updated)
	return svc, err
}

// CreateNote adds a new internal note to a work order.
func (s WorkOrder) CreateNote(ctx context.Context, wo int, msg string) (*model.Note, error) {
	note := &model.Note{
		Note: msg,
	}
	err := s.createNote.
		QueryRowContext(ctx, wo, msg).
		Scan(&note.ID, &note.Created)
	if err != nil && errors.Is(err, sql.ErrNoRows) {
		return nil, fmt.Errorf("work order %d does not exist or an error occured", wo)
	}
	return note, err
}

// CreateServiceLineItem adds a new line item to the work order that points to a
// service to be performed.
func (s WorkOrder) CreateServiceLineItem(ctx context.Context, newLi model.NewServiceLineItem) (*model.ServiceLineItem, error) {
	li := &model.ServiceLineItem{
		Service: &model.Service{
			ID: newLi.Service,
		},
		Dur:       newLi.Dur,
		Discount:  newLi.Discount,
		FinalCost: newLi.FinalCost,
	}
	err := s.createServiceLineItem.
		QueryRowContext(ctx, newLi.Wo, newLi.Service, newLi.Dur, newLi.Discount, newLi.FinalCost).
		Scan(&li.ID, &li.Created, &li.Updated)
	if err != nil && errors.Is(err, sql.ErrNoRows) {
		return nil, fmt.Errorf("work order %d or service %d does not exist or an error occured", newLi.Wo, newLi.Service)
	}
	return li, err
}

// CreatePartLineItem adds a new line item to the work order that points to a
// part or other item in inventory to be sold.
func (s WorkOrder) CreatePartLineItem(ctx context.Context, newLi model.NewPartLineItem) (*model.PartLineItem, error) {
	li := &model.PartLineItem{
		Item: &model.Item{
			ID: newLi.Item,
		},
		Cost:      newLi.Cost,
		Discount:  newLi.Discount,
		FinalCost: newLi.FinalCost,
	}
	err := s.createPartLineItem.
		QueryRowContext(ctx, newLi.Wo, newLi.Item, newLi.Cost, newLi.Discount,
			newLi.FinalCost).
		Scan(&li.ID, &li.Created, &li.Updated)
	if err != nil && errors.Is(err, sql.ErrNoRows) {
		return nil, fmt.Errorf("work order %d or item %d does not exist or an error occured", newLi.Wo, newLi.Item)
	}
	return li, err
}

// CreateWorkOrder creates a new work order in the system.
func (s WorkOrder) CreateWorkOrder(ctx context.Context, newWO model.NewWorkOrder) (*model.WorkOrder, error) {
	wo := &model.WorkOrder{
		Status:   model.WOStatusWorkNew,
		Customer: &model.Customer{ID: newWO.Customer},
		CustReq:  newWO.CustReq,
	}
	if newWO.Status != nil {
		wo.Status = *newWO.Status
	}
	if newWO.NoteToCust != nil {
		wo.NoteToCust = *newWO.NoteToCust
	}
	if newWO.Bike != nil {
		wo.Bike = &model.Bike{ID: *newWO.Bike}
	}
	var err error
	// TODO: It seems like there should be an obvious way to combine this into one
	// query; maybe I'm just tired and not understanding join types.
	if newWO.Bike != nil {
		err = s.createWOBike.
			QueryRowContext(ctx, newWO.Status, newWO.Bike, newWO.Customer, newWO.CustReq,
				newWO.NoteToCust).
			Scan(&wo.ID, &wo.Created, &wo.Updated)
	} else {
		err = s.createWONoBike.
			QueryRowContext(ctx, newWO.Status, newWO.Customer, newWO.CustReq,
				newWO.NoteToCust).
			Scan(&wo.ID, &wo.Created, &wo.Updated)
	}
	if err != nil && errors.Is(err, sql.ErrNoRows) {
		return nil, fmt.Errorf("bike or customer does not exist or an error occured")
	}
	return wo, err
}
