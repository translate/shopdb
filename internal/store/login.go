package store

import (
	"context"
	"crypto/rand"
	"database/sql"
	"encoding/base64"
	"net"
	"net/http"
	"strconv"
	"time"

	"code.soquee.net/login"
	"golang.org/x/net/xsrftoken"
)

// Login performs operations on the logins table.
type Login struct {
	getLogin      *sql.Stmt
	getUserLogins *sql.Stmt
	logout        *sql.Stmt
	newLogin      *sql.Stmt
	clearLogins   *sql.Stmt
	clearBefore   *sql.Stmt
	key           string
}

// NewLogin creates a new Login that performs its operations on the given
// database.
func NewLogin(ctx context.Context, db *sql.DB, key string) (Login, error) {
	var err error
	store := Login{
		key: key,
	}

	store.logout, err = db.PrepareContext(ctx, `DELETE FROM logins WHERE owner=$1 AND id=$2`)
	if err != nil {
		return store, err
	}

	store.newLogin, err = db.PrepareContext(ctx, `INSERT INTO logins (owner, mac, ip, ua) VALUES ($1, $2, $3, $4) RETURNING id`)
	if err != nil {
		return store, err
	}

	store.getUserLogins, err = db.PrepareContext(ctx, `
	SELECT id, ip, ua, created_at FROM logins WHERE owner=$1`)
	if err != nil {
		return store, err
	}

	store.clearLogins, err = db.PrepareContext(ctx, `
	DELETE FROM logins WHERE owner=$1 AND id<>$2`)
	if err != nil {
		return store, err
	}

	store.clearBefore, err = db.PrepareContext(ctx, `
	DELETE FROM logins WHERE owner=$1 AND created_at<$2`)
	if err != nil {
		return store, err
	}

	store.getLogin, err = db.PrepareContext(ctx, `UPDATE logins SET ip=$2, ua=$3 WHERE id=$1 RETURNING mac`)
	return store, err
}

const (
	loginTokenLen = 64
)

// Session represents information associated with a specific user login.
type Session struct {
	ID      int64
	Created time.Time
	IP      string
	UA      string
}

// GetUserLogins returns the sessions for the given user.
func (s Login) GetUserLogins(ctx context.Context, uid int) ([]Session, error) {
	rows, err := s.getUserLogins.QueryContext(ctx, uid)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var sessions []Session
	for rows.Next() {
		sess := Session{}
		err = rows.Scan(&sess.ID, &sess.IP, &sess.UA, &sess.Created)
		if err != nil {
			return sessions, err
		}
		sessions = append(sessions, sess)
	}

	return sessions, nil
}

func getIP(r *http.Request) string {
	remoteAddr := r.Header.Get("X-Forwarded-For")
	if remoteAddr == "" {
		var err error
		remoteAddr, _, err = net.SplitHostPort(r.RemoteAddr)
		if err != nil {
			remoteAddr = r.RemoteAddr
		}
	}
	return remoteAddr
}

// NewLogin creates an new login token for the provider user.
// If successful it returns the randomness that was used in the HMAC that was
// inserted into the datbase.
// This randomness can then be put in the login cookie and used to verify the
// MAC later to ensure that the login is still valid.
func (s Login) NewLogin(ctx context.Context, uid int, r *http.Request) (id int64, tok string, err error) {
	var n int
	buf := make([]byte, loginTokenLen)
	n, err = rand.Read(buf)
	if err != nil {
		return 0, "", err
	}
	if n != len(buf) {
		panic("store: failed to fill buffer with randomness")
	}
	tok = base64.RawURLEncoding.EncodeToString(buf)

	remoteAddr := getIP(r)
	userAgent := r.Header.Get("User-Agent")
	mac := xsrftoken.Generate(s.key+tok, strconv.Itoa(uid), "login")
	err = s.newLogin.QueryRowContext(ctx, uid, mac, remoteAddr, userAgent).Scan(&id)
	return id, tok, err
}

// CheckLogin checks if there is a valid login for the provided uid and token.
// It updates the last seen time if a login does exist and the IP address and
// user agent that were used.
func (s Login) CheckLogin(ctx context.Context, id int64, uid int, tok string, r *http.Request) (bool, error) {
	var mac string
	userAgent := r.Header.Get("User-Agent")
	remoteAddr := getIP(r)
	err := s.getLogin.QueryRowContext(ctx, id, remoteAddr, userAgent).Scan(&mac)
	switch {
	case err == sql.ErrNoRows:
		return false, nil
	case err != nil:
		return false, err
	}

	return xsrftoken.ValidFor(mac, s.key+tok, strconv.Itoa(uid), "login", login.LoginCookieDuration), nil
}

// ClearLogins invalidates all sessions for the given user except the one with
// the current ID.
// To clear all sessions, set id to a negative number.
func (s Login) ClearLogins(ctx context.Context, tx *sql.Tx, uid int, id int64) error {
	if tx != nil {
		_, err := tx.Stmt(s.clearLogins).ExecContext(ctx, uid, id)
		return err
	}
	_, err := s.clearLogins.ExecContext(ctx, uid, id)
	return err
}

// ClearBefore removes all logins that were created before the given time.
// Normally this would be used to remove expired logins.
func (s Login) ClearBefore(ctx context.Context, tx *sql.Tx, uid int, before time.Time) error {
	if tx != nil {
		_, err := tx.Stmt(s.clearBefore).ExecContext(ctx, uid, before)
		return err
	}
	_, err := s.clearBefore.ExecContext(ctx, uid, before)
	return err
}

// Logout removes the provided login ID from the database.
func (s Login) Logout(ctx context.Context, uid int, id int64) error {
	_, err := s.logout.ExecContext(ctx, uid, id)
	return err
}
