package store

import (
	"context"
	"database/sql"
	"errors"
	"fmt"

	"code.soquee.net/shopdb/internal/graph/model"
)

// Bikes performs operations on the bikes table.
type Bikes struct {
	db              *sql.DB
	getBikeByIDorSN *sql.Stmt
	getBikeByWO     *sql.Stmt

	newBike *sql.Stmt
}

// NewBikes creates a new Bike that performs its operations on the
// given database.
func NewBikes(ctx context.Context, db *sql.DB) (Bikes, error) {
	var err error
	store := Bikes{
		db: db,
	}

	store.getBikeByIDorSN, err = db.PrepareContext(ctx, `
SELECT num,make,model,color,sn,created_at,updated_at FROM bikes WHERE (0=$1 OR num=$1) AND (''=$2 OR sn=$2)`)
	if err != nil {
		return store, fmt.Errorf("error preparing getBikeByID: %q", err)
	}
	store.getBikeByWO, err = db.PrepareContext(ctx, `
SELECT b.num,b.make,b.model,b.color,b.sn,b.created_at,b.updated_at FROM bikes b
	LEFT JOIN workorders AS wo ON (b.id=wo.bike)
	WHERE wo.num=$1`)
	if err != nil {
		return store, fmt.Errorf("error preparing getBikeByID: %q", err)
	}

	store.newBike, err = db.PrepareContext(ctx, `
	INSERT INTO bikes (make, model, color, sn, customer)
		SELECT $1, $2, $3, $4, c.id FROM customers c WHERE c.num=$5
		RETURNING num, make, model, color, sn, created_at, updated_at
`)
	if err != nil {
		return store, fmt.Errorf("error preparing newBike: %q", err)
	}

	return store, nil
}

// DB returns the database that the store was created with.
func (s Bikes) DB() *sql.DB {
	return s.db
}

// GetBikeByID returns the given bike or nil if no such bike exists.
func (s Bikes) GetBikeByID(ctx context.Context, num int) (*model.Bike, error) {
	bike := &model.Bike{}
	err := s.getBikeByIDorSN.QueryRowContext(ctx, num, "").Scan(&bike.ID, &bike.Make, &bike.Model, &bike.Color, &bike.Serial, &bike.Created, &bike.Updated)
	if err != nil && errors.Is(err, sql.ErrNoRows) {
		return nil, nil
	}
	return bike, err
}

// GetBikeBySN returns the given bike or nil if no such bike exists.
func (s Bikes) GetBikeBySN(ctx context.Context, serial string) (*model.Bike, error) {
	bike := &model.Bike{}
	err := s.getBikeByIDorSN.QueryRowContext(ctx, 0, serial).Scan(&bike.ID, &bike.Make, &bike.Model, &bike.Color, &bike.Serial, &bike.Created, &bike.Updated)
	if errors.Is(err, sql.ErrNoRows) {
		return nil, nil
	}
	return bike, err
}

// GetBikeByWO returns the bike on the given work order or nil if no such work
// order exists.
func (s Bikes) GetBikeByWO(ctx context.Context, wo int) (*model.Bike, error) {
	bike := &model.Bike{}
	err := s.getBikeByWO.QueryRowContext(ctx, wo).Scan(
		&bike.ID, &bike.Make, &bike.Model, &bike.Color, &bike.Serial,
		&bike.Created, &bike.Updated)
	if errors.Is(err, sql.ErrNoRows) {
		return nil, nil
	}
	return bike, err
}

// CreateBike inserts a new bike and returns the resulting value.
func (s Bikes) CreateBike(ctx context.Context, newBike model.NewBike) (*model.Bike, error) {
	bike := &model.Bike{}
	err := s.newBike.
		QueryRowContext(ctx, newBike.Make, newBike.Model, newBike.Color,
			newBike.Serial, newBike.Owner).
		Scan(&bike.ID, &bike.Make, &bike.Model, &bike.Color, &bike.Serial,
			&bike.Created, &bike.Updated)
	return bike, err
}
