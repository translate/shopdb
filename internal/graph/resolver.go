package graph

import (
	"log"

	"code.soquee.net/shopdb/internal/store"
)

type Resolver struct {
	Store  *store.DB
	Logger *log.Logger
	Debug  *log.Logger
}
