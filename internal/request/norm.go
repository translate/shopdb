// Package request contains middleware for dealing with API and UI requests.
package request

import (
	"errors"
	"net/http"
	"net/url"
	"strings"
	"unicode"

	"golang.org/x/text/runes"
	"golang.org/x/text/secure/precis"
	"golang.org/x/text/transform"

	"code.soquee.net/mux"
)

// Constants used for validating passwords.
const (
	MinPasswordLength = 12
	MaxPasswordLength = 1000
)

// UnicodeVersion is the Unicode version from which the tables used by this
// package are derived.
const UnicodeVersion = precis.UnicodeVersion

// Errors that may be returned by validation functions in this package.
var (
	ErrLongPassword  = errors.New("request: password is too long")
	ErrShortPassword = errors.New("request: password is too short")
)

// Disallowed are extra characters that are not allowed by URLProfile.
// They are mostly taken from RFC 3986 §2.2 except for '.' and '%' which are
// reserved by us.
const Disallowed = `:/\?#[]@!#&'()*+,;=.%`

// URLProfile is a version of the Username Case Mapped profile with additional
// restricted characters.
// It is used for user, org, and project names.
var URLProfile = precis.NewRestrictedProfile(precis.UsernameCaseMapped,
	runes.Predicate(func(r rune) bool {
		return strings.ContainsRune(Disallowed, r)
	}),
)

// NormalizeForm calls ParseForm on the request and then attempts to normalize
// the provided form values.
// It does not attempt to normalize multipart form data or URL query parameters.
func NormalizeForm(r *http.Request, opts ...Option) (badkey string, err error) {
	err = r.ParseForm()
	if err != nil {
		return "", err
	}

	for _, o := range opts {
		badkey, err = o(r.PostForm)
		if err != nil {
			return badkey, err
		}
	}

	return "", nil
}

// Username returns a username from the given displayname by removing spaces and
// applying URLProfile.
func Username(displayname string) (string, error) {
	usernameTransform := transform.Chain(runes.Remove(runes.In(unicode.Space)), URLProfile.NewTransformer())
	username, _, err := transform.String(usernameTransform, displayname)
	return username, err
}

// getsetter is the interface implemented by types with a Get and Set method.
type getsetter interface {
	Set(string, string)
	Get(string) string
}

// Option configures form normalization.
type Option func(getsetter) (string, error)

// Name is an Option that normalizes a form parameter using URLProfile.
func Name(key string) Option {
	return func(v getsetter) (string, error) {
		val, err := URLProfile.String(v.Get(key))
		if err != nil {
			return key, err
		}
		v.Set(key, val)
		return key, nil
	}
}

// DisplayName is an Option that normalizes a form parmeter using the Username
// Case Preserved PRECIS profile on each individual word ("userpart" in PRECIS
// lingo) in the name and normalizing spaces to ASCII Space.
func DisplayName(key string) Option {
	return func(v getsetter) (string, error) {
		displayname := v.Get(key)

		// TODO: This is expensive.
		parts := strings.FieldsFunc(displayname, unicode.IsSpace)
		var err error
		for i, part := range parts {
			parts[i], err = precis.UsernameCasePreserved.String(part)
			if err != nil {
				return key, err
			}
		}
		displayname = strings.Join(parts, " ")

		v.Set(key, displayname)
		return key, nil
	}
}

// UnixLineEndings normalizes "\r\n" and "\r" to "\n".
func UnixLineEndings(key string) Option {
	return func(v getsetter) (string, error) {
		val := v.Get(key)
		// TODO: do this in one iteration with a transformer.
		val = strings.Replace(val, "\r\n", "\n", -1)
		val = strings.Replace(val, "\r", "\n", -1)
		v.Set(key, val)
		return key, nil
	}
}

// Password is a normalizer that applies the precise OpaqueString profile and
// checks password length limits.
// This is mostly used for passwords, but may also be useful for normalizing any
// generic string where flexibility is required but where basic constraints (eg.
// the string may not be empty) are still desirable.
func Password(key string) Option {
	return func(v getsetter) (string, error) {
		s, err := precis.OpaqueString.String(v.Get(key))
		if err != nil {
			return key, err
		}
		if len(s) > MaxPasswordLength {
			return key, ErrLongPassword
		}
		if len(s) < MinPasswordLength {
			return key, ErrShortPassword
		}
		v.Set(key, s)
		return key, nil
	}
}

// OpaqueString is a normalizer that applies the precise OpaqueString profile.
// While OpaqueString is mostly applied to passwords, the Password option
// provides other validations and should generally be used instead.
func OpaqueString(key string) Option {
	return func(v getsetter) (string, error) {
		s, err := precis.OpaqueString.String(v.Get(key))
		if err != nil {
			return key, err
		}
		v.Set(key, s)
		return key, nil
	}
}

// Normalize returns a middleware used to normalize common path parameters.
func Normalize(badRequest http.HandlerFunc, normParams []string) func(http.HandlerFunc) http.HandlerFunc {
	return func(h http.HandlerFunc) http.HandlerFunc {
		return func(w http.ResponseWriter, r *http.Request) {
			var anyNormalized bool
			for _, param := range normParams {
				pinfo := mux.Param(r, param)
				if pinfo.Value == nil {
					continue
				}

				normed, err := url.PathUnescape(pinfo.Raw)
				if err != nil {
					badRequest.ServeHTTP(w, r)
					return
				}
				normed, err = URLProfile.String(normed)
				if err != nil {
					badRequest.ServeHTTP(w, r)
					return
				}

				if normed == pinfo.Raw {
					continue
				}

				anyNormalized = true
				r = mux.WithParam(r, param, normed)
			}

			if !anyNormalized {
				h.ServeHTTP(w, r)
				return
			}

			newPath, err := mux.Path(r)
			if err != nil {
				badRequest.ServeHTTP(w, r)
				return
			}

			http.Redirect(w, r, newPath, http.StatusPermanentRedirect)
		}
	}
}
