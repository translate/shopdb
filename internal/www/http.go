package www

import (
	"net/http"
	"path"
)

// publicMIME overrides the Content-Type header for things in the /public tree.
// It should not be used anywhere that user uploaded files may be rendered.
func publicMIME(h http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		header := w.Header()
		switch path.Ext(r.URL.Path) {
		case ".js":
			header.Set("Content-Type", "application/javascript; charset=utf-8")
		case ".css":
			header.Set("Content-Type", "text/css; charset=utf-8")
		case ".json":
			header.Set("Content-Type", "application/json; charset=utf-8")
		case ".svg", ".svgz":
			header.Set("Content-Type", "image/svg+xml; charset=utf-8")
		case ".wasm":
			header.Set("Content-Type", "application/wasm")
		default:
			header.Set("Content-Type", "Content-Type: application/octet-stream")
		}
		h.ServeHTTP(w, r)
	}
}

// securityHeaders is a middleware that sets various comment security measures
// such as a content security policy, strict transport security, etc.
// It also redirects all requests with an X-Forwarded-Proto heading of "http" to
// the same resource with the https: scheme.
// It should be applied as the root middleware.
//
// This function is considered security sensitive.
// Be extra careful with your code review in this function (even if you can't
// see how the change would cause a security issue).
func securityHeaders(domain string, h http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		header := w.Header()
		header.Set("Content-Security-Policy", `default-src 'none'; manifest-src 'self'; script-src 'self'; object-src 'none'; style-src 'self'; img-src 'self' data:; media-src 'none'; frame-src 'none'; font-src 'self'; connect-src 'self'; frame-ancestors 'none'; base-uri 'none'; form-action 'self'`)
		header.Set("Referrer-Policy", "no-referrer")
		header.Set("Strict-Transport-Security", "max-age=63072000; includeSubDomains; preload")
		header.Set("X-Content-Type-Options", "nosniff")
		header.Set("X-Frame-Options", "DENY")

		// Set text/html as the default Content-Type.
		// It is up to individual handlers or reverse proxies to override this if
		// required (eg. to comply with the X-Content-Type-Options: nosniff set
		// above).
		header.Set("Content-Type", "text/html; charset=utf-8")

		if r.Header.Get("X-Forwarded-Proto") == "http" {
			r.URL.Scheme = "https"
			r.URL.Host = domain
			http.Redirect(w, r, r.URL.String(), http.StatusPermanentRedirect)
			return
		}

		h.ServeHTTP(w, r)
	}
}
